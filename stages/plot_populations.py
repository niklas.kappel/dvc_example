import os

import matplotlib.pyplot as plt
import pandas as pd

OUTPUT_DIR = "stage_outputs/plot_populations/"


def pick_lines(file_name, line_numbers):
    with open(file_name) as file:
        lines = [line for i, line in enumerate(file) if i in line_numbers]
    return lines


def read_col_names(file_name, line_number):
    header_line = pick_lines(file_name, [line_number])[0]
    header = header_line[1:].strip().split()
    return header


def read_population_file(file_name):
    names = read_col_names(file_name, line_number=1)
    population = pd.read_csv(file_name, comment="#", sep=r"\s+", names=names)
    return population


def main():
    os.makedirs(OUTPUT_DIR, exist_ok=True)

    population_1 = read_population_file("stage_outputs/run_md/md_population1vstime.dat")
    population_2 = read_population_file("stage_outputs/run_md/md_population2vstime.dat")
    fig, ax = plt.subplots()
    ax.plot(
        population_1.iloc[:, 0],
        population_1.iloc[:, 1],
        label="pop1",
        linestyle="dotted",
    )
    ax.plot(
        population_2.iloc[:, 0],
        population_2.iloc[:, 1],
        label="pop2",
        linestyle="dotted",
    )
    ax.set_xlabel("Time")
    ax.set_ylabel("Population")
    ax.legend()
    fig.tight_layout()
    fig.savefig(OUTPUT_DIR + "populations.png")


if __name__ == "__main__":
    main()
