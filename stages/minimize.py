import os

import dvc.api
from lammps import PyLammps

OUTPUT_DIR = "stage_outputs/minimize/"


def main():
    # Read the large data file.
    with open("data/very_big_data_file.txt") as f:
        print(f.read())

    # Read the parameters from dvc.yaml.
    params = dvc.api.params_show()["minimize"]
    print(params)

    os.makedirs(OUTPUT_DIR, exist_ok=True)
    lmp = PyLammps()

    # 1) Initialization
    lmp.units(params["units"])
    lmp.dimension("3")
    lmp.atom_style("atomic")
    lmp.pair_style("lj/cut 2.5")
    lmp.boundary("p p p")

    # 2) System definition
    lmp.region("simulation_box block -10 10 -10 10 -10 10")
    lmp.create_box("2 simulation_box")
    lmp.region("region_cylinder_in cylinder z 0 0 5 INF INF side in")
    lmp.region("region_cylinder_out cylinder z 0 0 5 INF INF side out")
    lmp.create_atoms("1 random 128 341341 region_cylinder_out")
    lmp.create_atoms("2 random 16 127569 region_cylinder_in")

    # 3) Simulation settings
    lmp.mass("1", params["mass_1"])
    lmp.mass("2", params["mass_2"])
    lmp.pair_coeff("1 1 1.0 1.0")
    lmp.pair_coeff("2 2 0.5 3.0")
    lmp.neigh_modify("every 1 delay 0 check yes")

    # 4) Visualization
    lmp.thermo("10")
    lmp.thermo_style("custom step temp press pe ke etotal")
    lmp.dump("my_dump all atom 10", OUTPUT_DIR + "min.lammpstrj")

    # 5) Run
    lmp.minimize("1.0e-4 1.0e-6 1000 10000")
    lmp.write_data(OUTPUT_DIR + "min.data")


if __name__ == "__main__":
    main()
