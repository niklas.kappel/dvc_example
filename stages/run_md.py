import os

import dvc.api
from lammps import PyLammps

INPUT_DIR = "stage_outputs/minimize/"
OUTPUT_DIR = "stage_outputs/run_md/"


def main():
    # Read the parameters from dvc.yaml.
    params = dvc.api.params_show()["run_md"]
    print(params)

    os.makedirs(OUTPUT_DIR, exist_ok=True)
    lmp = PyLammps()

    # 1) Initialization
    lmp.units("lj")
    lmp.dimension("3")
    lmp.atom_style("atomic")
    lmp.pair_style("lj/cut 2.5")
    lmp.boundary("p p p")

    # 2) System definition
    lmp.read_data(INPUT_DIR + "min.data")
    lmp.region("region_cylinder_in cylinder z 0 0 5 INF INF side in")
    lmp.group("group_type_1 type 1")
    lmp.group("group_type_2 type 2")

    # 3) Simulation settings
    lmp.neigh_modify("every 1 delay 0 check no")

    # 4) Visualization
    output_delay = 1000

    # Thermo output
    lmp.thermo(output_delay)
    lmp.thermo_style("custom step temp press pe ke etotal")
    lmp.thermo_modify("norm no")

    # Write atom-wise data.
    lmp.dump(
        "my_dump all custom",
        output_delay,
        OUTPUT_DIR + "md.lammpstrj",
        "id type x y z fx fy fz",
    )

    # Write thermo data.
    thermo_file = OUTPUT_DIR + "md_thermo.dat"
    lmp.variable("step equal step")
    lmp.variable("pe equal pe")
    lmp.print('"# step pe"', "file", thermo_file, "screen no")
    lmp.fix(
        "my_print all print",
        output_delay,
        '"${step} ${pe}"',
        "append",
        thermo_file,
        "screen no",
    )

    # Write cylinder population data.
    lmp.variable("number_type1_in equal count(group_type_1,region_cylinder_in)")
    lmp.variable("number_type2_in equal count(group_type_2,region_cylinder_in)")
    lmp.fix(
        "myat1 all ave/time 10 200 2000 v_number_type1_in file",
        OUTPUT_DIR + "md_population1vstime.dat",
    )
    lmp.fix(
        "myat2 all ave/time 10 200 2000 v_number_type2_in file",
        OUTPUT_DIR + "md_population2vstime.dat",
    )

    # 5) Run
    lmp.velocity("all create 1.0 4928459 mom yes rot yes dist gaussian")
    lmp.fix("mynve all nve")
    lmp.fix("mylgv all langevin 1.0 1.0 0.1 1530917 zero yes")
    lmp.timestep(params["timestep"])
    lmp.run(params["step_count"])
    lmp.write_data(OUTPUT_DIR + "md.data")


if __name__ == "__main__":
    main()
