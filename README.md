# DVC example

## Project overview

This repo contains an example workflow in the form of a [DVC pipeline](https://dvc.org/doc/start/data-pipelines/data-pipelines). The workflow performs an MD simulation of an LJ fluid in LAMMPS and consists of three stages (aka nodes):

1. Create a cylindrical region in a cubic simulation box. Randomly place atoms of one type inside the cylinder and atoms of another type outside the cylinder. Minimize the energy. Output the energy-minimized configuration.
1. Starting from the minimized configuration, perform MD at constant temperature. Output the time evolution of the populations of the two atom types inside the cylinder.
1. Plot the populations over the simulation time.

Each stage is defined by a Python script in the `stages/` directory. The stages write their output to the `stage_outputs/` directory.

The first stage also expects a file in the `data/` directory to be present. This file is an example for a large externally generated data file that is added to the project and tracked by DVC. DVC tracked files (potentially very large) are ignored by Git (see `data/.gitignore`). Instead, Git tracks a placeholder file (very small, see `data/very_big_data_file.txt.dvc`).

The first two stages also read parameters (e.g. the timestep) from the file `params.yaml` using the DVC API. Name and placement of this parameter file is a convention introduced by DVC.

The workflow itself is defined in the file `dvc.yaml`. Here, each stage declares a shell command that it wants to run (`cmd`), its file dependencies (`deps`), its parameter dependencies (`params`), and its outputs (`outs`). The whole workflow can be run using the command `dvc repro`.

Because later stages depend on the outputs of earlier stages, together they form a directed acyclic workflow graph. The `dvc repro` command walks through this graph, and executes the stage commands if and only if this is necessary, i.e. if the file or parameter dependencies of a stage have changed or if the output files of the stage are missing. Any stage outputs are automatically tracked by DVC and ignored by Git.

The file `environment.yaml` defines the conda environment required to run the workflow.

In summary, the workflow is well-defined in `dvc.yaml`, making it reproducible. All input and output files are tracked by DVC, making it easy to back them up or share them.

## Sharing data

Just like Git synchronizes (small code) files in your workspace with previous versions in a local Git cache and a remote (e.g. on GitLab), DVC synchronizes (large data) files in your workspace with previous versions in a local DVC cache and a remote (e.g. a directory on int-nano).

External data files can be added to the DVC cache using `dvc add`. Files referenced in `dvc.yaml` are automatically added. Files can be pushed to and pulled from the DVC remote using `dvc push` and `dvc pull`. Files are automatically pulled and/or restored from the cache by `dvc repro` when the dependencies of a stage have not changed and its outputs are missing in the workspace.

## Setting up DVC projects

To set up this project, the following manual steps were necessary:

1. (Create a conda environment that includes `dvc` and all dependencies.)
1. (Create a project directory and run `git init` and `dvc init` there.)
1. Obtain the file `data/very_big_data_file.txt` and add it to DVC.
1. Write the stage scripts.
1. Write the `params.yaml` file.
1. Write the `dvc.yaml` file.

Note that this requires specifying a lot of duplicate information. We have to:

- match stage scripts in `stages/` and `dvc.yaml`.
- match I/O file paths in the stage scripts and `dvc.yaml`.
- match parameter sections in stage scripts, `dvc.yaml` and `params.yaml`.
- match parameter names in stage scripts and `params.yaml`.

When we want to change a detail about the project, we need to change it in all locations that reference it. The useful (but less mature) [ZnTrack package](https://github.com/zincware/ZnTrack) provides an alternative way to create DVC workflows in a more convenient way.
